<?php
/**
 * Template Name: Sidebar Template
 */

use Backstage\View\Element;
use ChildTheme\Sidebar\SidebarPage;
use ChildTheme\Support\PreferredDimensionsHandler;

$SidebarPage = SidebarPage::createFromGlobal();
?>

<div class="content-section content-section--hero content-section--has-bg content-section--mb-double">
    <?php if (($image = $SidebarPage->featuredImage()) instanceof WP_Image): ?>
        <?php $image->width(PreferredDimensionsHandler::DETAIL_WIDTH)->height(PreferredDimensionsHandler::DETAIL_HEIGHT); ?>
        <span class="content-background-image content-background-image--top">
            <span class="content-background-image__images">
                <span class="content-background-image__img content-background-image__img--desktop" style='background-image: url(<?= $image->url; ?>);'></span>
            </span>
        </span>
    <?php endif; ?>
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <div class="styled-heading">
                    <?php
                        if (($whitespace_position = strpos(($title = $SidebarPage->title()), ' ')) !== -1) {
                            $styled_text = substr($title, 0, $whitespace_position);
                            $heading_text = substr($title, $whitespace_position + 1);
                            echo sprintf('<strong class="heading heading--medium heading--styled">%s</strong>', $styled_text);
                            echo Element::create('h1', $heading_text, ['class' => 'heading heading--large']);
                        } else {
                            echo Element::create('h1', $title, ['class' => 'heading heading--large']);
                        }
                    ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sidebar-template">
    <div class="sidebar-template__container container">
        <div class="sidebar-template__main">
            <?= $SidebarPage->content(false); ?>
        </div>
        <div class="sidebar-template__sidebar">
            <section class="content-section content-section--mb-half">
                <div class="content-section__container container">
                    <div class="content-row row">
                        <?php dynamic_sidebar($SidebarPage->sidebar_name); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>

