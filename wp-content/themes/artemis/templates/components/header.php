<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use ChildTheme\Options\GlobalOptions;
?>

<div class="header-nav__lock-up">
    <div class="header-nav__logo">
        <a class="header-nav__logo-link" href="<?= home_url() ?>">
            <?php if ($header_image = GlobalOptions::headerBrandImage()): ?>
                <?= $header_image
                    ->height(75)
                    ->width(75)
                    ->css_class('header-nav__logo-image') ?>
            <?php else: ?>
                <strong><?php bloginfo('name'); ?></strong>
            <?php endif; ?>
        </a>
    </div>
    <?php if (has_nav_menu('primary_navigation')): ?>
        <div class="header-nav__menu">
            <?= NavMenuView::create('primary_navigation'); ?>
        </div>
    <?php endif; ?>
</div>
<header class="header-nav" data-gtm="Header">
    <div class="header-nav__container container-fluid">
        <ul class="header-nav__list list--inline">
            <?php if ($phone = GlobalOptions::contactPhone()): ?>
                <li class="header-nav__list-item header-nav__list-item--phone">
                    <a href="<?= sprintf('tel:%s', $phone); ?>">
                        <?= sprintf('%s <span>%s</span>', new IconView(['icon_name' => 'phone']), $phone); ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if ($email = GlobalOptions::contactEmail()): ?>
                <li class="header-nav__list-item header-nav__list-item--email">
                    <a href="<?= sprintf('mailto:%s', $email); ?>">
                        <?= sprintf('%s <span>%s</span>', new IconView(['icon_name' => 'email']), $email); ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                    <li class="header-nav__list-item">
                        <a href="<?= $SocialIcon->getUrl(); ?>" class="link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                            <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
    </div>
</header>
