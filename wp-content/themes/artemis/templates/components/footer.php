<?php

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\Modal\ModalView;
use Backstage\SetDesign\NavMenu\NavMenuView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use ChildTheme\Options\GlobalOptions;
use Backstage\Support\DateTime;
?>

<section class="content-section content-section--width-narrow content-section--mb-none content-section--has-bg content-section--darker content-section--tpad-double content-section--bpad-double">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 text--center content-column--last">
                <div class="content-column__inner">
                    <h2 class="heading heading--default">Subscribe To the Live Love Artemis Weekly Newsletter To Be In the Know</h2>
                    <div class="column-text">
                        <p>Be the first to hear about our new video releases, new podcast episodes, new listings, open houses, and more!</p>
                    </div>
                    <?= do_shortcode('[gravityform id="2" title="false"]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section anchor content-section--has-bg content-section--darker content-section--tpad-none content-section--bpad-none content-section--width-full content-section--mb-none">
    <div class="container-fluid content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <?= do_shortcode('[instagram-feed num=6 cols=6]'); ?>
                </div>
            </div>
        </div>
    </div>
</section>
<footer class="footer-nav">
    <div class="footer-nav__container container">
        <?php if (($image = GlobalOptions::additionalBrandImage()) instanceof WP_Image): ?>
            <div class="footer-nav__image text--center">
                <?= $image->width(400); ?>
            </div>
        <?php endif; ?>
        <ul class="footer-nav__list list--inline">
            <?php if ($phone = GlobalOptions::contactPhone()): ?>
                <li class="footer-nav__list-item footer-nav__list-item--phone">
                    <a href="<?= sprintf('tel:%s', $phone); ?>">
                        <?= sprintf('%s <span>%s</span>', new IconView(['icon_name' => 'phone']), $phone); ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if ($email = GlobalOptions::contactEmail()): ?>
                <li class="footer-nav__list-item footer-nav__list-item--email">
                    <a href="<?= sprintf('mailto:%s', $email); ?>">
                        <?= sprintf('%s <span>%s</span>', new IconView(['icon_name' => 'email']), $email); ?>
                    </a>
                </li>
            <?php endif; ?>
            <?php if (!empty($social_icons = GlobalOptions::socialIcons())): ?>
                <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                    <li class="footer-nav__list-item">
                        <a href="<?= $SocialIcon->getUrl(); ?>" class="link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                            <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            <?php endif; ?>
        </ul>
        <?php if (has_nav_menu('footer_navigation')): ?>
            <div class="footer-nav__menu">
                <?= NavMenuView::createList('footer_navigation'); ?>
            </div>
        <?php endif; ?>
        <div class="footer-nav__copyright">
            &copy; <?= new DateTime('now', 'Y'); ?>. <span class="text--uppercase">Live Love Artemis</span>. All rights reserved. Sitemap | Real Estate Website Design by <a href="https://vincentragosta.io" target="_blank">Vincent Ragosta</a>
        </div>
    </div>
</footer>


<?= ModalView::load('newsletter','box', do_shortcode(apply_filters('orchestrator/newsletter-shortcode', '[gravityform id="1" title=true description=true ajax=true]'))); ?>
<?= ModalView::unloadAll(); ?>
