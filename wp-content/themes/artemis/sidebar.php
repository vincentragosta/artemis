<aside class="sidebar">
    <section class="content-section content-section--mb-half">
        <div class="content-section__container container">
            <div class="content-row row">
                <?php dynamic_sidebar('Primary Sidebar'); ?>
            </div>
        </div>
    </section>
</aside>

