<?php

namespace ChildTheme\TeamMember;

use Backstage\Repositories\PostRepository;

/**
 * Class TeamMemberRepository
 * @package ChildTheme\TeamMember
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class TeamMemberRepository extends PostRepository
{
    protected $model_class = TeamMember::class;
}
