<?php

namespace ChildTheme\TeamMember;

use Backstage\Models\PostBase;
use Backstage\SetDesign\SocialIcons\SocialIcon;

/**
 * Class TeamMember
 * @package ChildTheme\TeamMember
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $role
 * @property string $phone
 * @property string $email
 * @property \WP_Image $secondary_image
 * @property array $social_icons
 */
class TeamMember extends PostBase
{
    const POST_TYPE = 'team-member';

    protected function getSecondaryImage()
    {
        $image = $this->field('secondary_image');
        return \WP_Image::get_by_attachment_id($image);
    }

    protected function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->field('social_icons'));
    }
}
