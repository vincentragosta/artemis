<?php

namespace ChildTheme\Search;

use Backstage\SetDesign\Icon\IconView;

/**
 * Class SearchController
 * @package ChildTheme\Search
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class SearchController
{
    const FORM_MARKUP = '
        <form role="search" method="get" class="search-form" action="%s">
            <label class="search-form__label">
                <span class="screen-reader-text">%s</span>
                <input type="search" class="search-form__field" placeholder="%s" value="%s" name="s" />
            </label>
            <button type="submit" class="button">%s</button>
        </form>';

    public function __construct()
    {
        add_filter('get_search_form', [$this, 'formMarkup']);
    }

    public function formMarkup($form)
    {
        return sprintf(
            static::FORM_MARKUP, esc_url(home_url('/')),
            _x('Search for:', 'label'),
            esc_attr_x('Search &hellip;', 'placeholder'),
            get_search_query(),
            new IconView(['icon_name' => 'search'])
        );
    }
}
