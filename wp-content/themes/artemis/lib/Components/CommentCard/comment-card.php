<?php
/**
 * Expected:
 * @var string $display_name
 * @var string $date
 * @var string $content
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($content) || empty($display_name) || empty($date)) {
    return '';
}
?>

<div <?= Util::componentAttributes('comment-card', $class_modifiers, $element_attributes); ?>>
    <h2 class="comment-card__author heading"><?= $display_name; ?></h2>
    <p class="comment-card__date"><?= $date; ?></p>
    <p class="comment-card__content"><?= $content; ?></p>
</div>
