<?php

namespace ChildTheme\Components\CommentCard;

use Backstage\View\Component;
use ChildTheme\Support\RelativeFormatConverter;

/**
 * Class CommentCardView
 * @package ChildTheme\Components\CommentCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $display_name
 * @property string $content
 * @property string $date
 */
class CommentCardView extends Component
{
    protected $name = 'comment-card';
    protected static $default_properties = [
        'display_name' => '',
        'content' => '',
        'date' => ''
    ];

    public function __construct(\WP_Comment $Comment)
    {
        parent::__construct([
            'display_name' => $Comment->comment_author,
            'content' => $Comment->comment_content,
            'date' => (new RelativeFormatConverter(strtotime($Comment->comment_date)))->getRelativeTime()
        ]);
    }
}
