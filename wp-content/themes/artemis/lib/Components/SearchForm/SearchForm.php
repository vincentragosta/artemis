<?php

namespace ChildTheme\Components\SearchForm;

use Backstage\VcLibrary\Support\Component;

/**
 * Class SearchForm
 * @package ChildTheme\Components\SearchForm
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SearchForm extends Component
{
    const NAME = 'Search Form';
    const TAG = 'search_form';

    protected $component_config = [
        'description' => 'Set the search form.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'show_settings_on_create' => false,
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => []
    ];

    protected function createView(array $atts)
    {
        return (string) get_search_form(false);
    }
}
