<?php

namespace ChildTheme\Components\LilyDevelopmentListingCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\LilyDevelopmentListing\LilyDevelopmentListing;
use ChildTheme\LilyDevelopmentListing\LilyDevelopmentListingRepository;

/**
 * Class LilyDevelopmentListingCard
 * @package ChildTheme\Components\LilyDevelopmentListingCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class LilyDevelopmentListingCard extends Component
{
    const NAME = 'Lily Development Listing Card';
    const TAG = 'lily_development_listing_card';
    const VIEW = LilyDevelopmentListingCardView::class;

    protected $component_config = [
        'description' => 'Create a card from a Lily Development listing.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'image' => [
                'type' => 'attach_image',
                'heading' => 'Image',
                'param_name' => 'image',
                'description' => 'Select a static image',
                'group' => 'Static'
            ],
            'logo' => [
                'type' => 'attach_image',
                'heading' => 'Logo',
                'param_name' => 'logo',
                'description' => 'Select a static logo',
                'group' => 'Static'
            ],
            'title' => [
                'type' => 'textfield',
                'heading' => 'Title',
                'param_name' => 'title',
                'description' => 'Set a static title.',
                'admin_label' => true,
                'group' => 'Static',
            ],
            'content' => [
                'type' => 'textarea',
                'heading' => 'Bio',
                'param_name' => 'bio',
                'description' => 'Set a static bio.',
                'group' => 'Static',
            ],
            'link' => [
                'type' => 'vc_link',
                'heading' => 'Link',
                'param_name' => 'link',
                'description' => 'Set a static link.',
                'group' => 'Static',
            ],
            'listings' => [
                'type' => 'dropdown',
                'heading' => 'Listings',
                'param_name' => 'lily_development_listing_id',
                'value' => '',
                'description' => 'Select a Listing',
                'admin_label' => true,
                'group' => 'Listing'
            ],
            'layout_options' => [
                'type' => 'checkbox',
                'heading' => 'Layout Options',
                'param_name' => 'layout_options',
                'value' => [
                    'Toggle Horizontal Display' => 'toggle-horizontal'
                ],
                'description' => 'Adjust the layout of the card.',
                'group' => 'Layout Options'
            ]
        ]
    ];

    protected function setupConfig()
    {
        parent::setupConfig();
        $this->setListings();
    }

    protected function setListings()
    {
        $options['-- Select Listing --'] = '';
        $LilyDevelopmentListingRepository = new LilyDevelopmentListingRepository();
        $listings = $LilyDevelopmentListingRepository->findAll();
        foreach ($listings as $Listing) {
            /** @var LilyDevelopmentListing $Listing */
            $options[$Listing->post()->post_title] = $Listing->ID;
        }
        $this->component_config['params']['listings']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        /* @var LilyDevelopmentListingCardView $ViewClass */
        $ViewClass = static::VIEW;
        if (!empty($atts['lily_development_listing_id'])) {
            return ($ViewClass::createFromListing($Listing = new LilyDevelopmentListing($atts['lily_development_listing_id'])))->setUrl($Listing->permalink())->classModifiers($atts['layout_options']);
        } else {
            $image = \WP_Image::get_by_attachment_id($atts['image']);
            $logo = \WP_Image::get_by_attachment_id($atts['logo']);
            $content = wpb_js_remove_wpautop($atts['bio']);
            if (!empty($atts['link'])) {
                $link = vc_build_link($atts['link']);
                return (new $ViewClass($image, $logo, $atts['title'], $content))->setUrl($link['url'])->classModifiers($atts['layout_options']);
            } else {
                return (new $ViewClass($image, $logo, $atts['title'], $content))->classModifiers($atts['layout_options']);
            }
        }
    }
}
