<?php
/**
 * Expected:
 * @var \WP_Image|boolean $image
 * @var \WP_Image|boolean $logo
 * @var string $title
 * @var string $bio
 * @var string $link_url
 * @var array $class_modifiers
 * @var array $element_attribbutes
 */

use Backstage\Util;
use Backstage\View\Link;

?>

<div <?= Util::componentAttributes('lily-development-listing-card', $class_modifiers, $element_attributes); ?>>
    <div class="lily-development-listing-card__row">
        <?php if ($image instanceof \WP_Image): ?>
            <div class="lily-development-listing-card__column">
                <div class="lily-development-listing-card__image">
                    <?php if ($link_url): ?>
                        <?= new Link($link_url, $image); ?>
                    <?php else: ?>
                        <?= $image; ?>
                    <?php endif;  ?>
                </div>
            </div>
        <?php endif; ?>
        <div class="lily-development-listing-card__column">
            <?php if ($logo instanceof \WP_Image): ?>
                <div class="lily-development-listing-card__logo">
                    <?php if ($link_url): ?>
                        <?= new Link($link_url, $logo); ?>
                    <?php else: ?>
                        <?= $logo; ?>
                    <?php endif;  ?>
                </div>
            <?php endif; ?>
            <?php if (!empty($title)): ?>
                <h2 class="lily-development-listing-card__heading heading heading--default">
                    <?= $link_url ? new Link($link_url, $title) : $title; ?>
                </h2>
            <?php endif; ?>
            <?php if (!empty($bio)): ?>
                <div class="lily-development-listing-card__bio">
                    <?= $bio; ?>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
