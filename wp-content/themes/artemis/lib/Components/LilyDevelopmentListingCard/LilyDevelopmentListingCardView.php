<?php

namespace ChildTheme\Components\LilyDevelopmentListingCard;

use Backstage\View\Component;
use ChildTheme\LilyDevelopmentListing\LilyDevelopmentListing;
use \WP_Image;
/**
 * Class LilyDevelopmentListingCardView
 * @package ChildTheme\Components\LilyDevelopmentListingCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property WP_Image $image
 * @property WP_Image $logo
 * @property string $title
 * @property string $bio
 * @property string $link_url
 */
class LilyDevelopmentListingCardView extends Component
{
    const IMAGE_WIDTH = 768;
    const LOGO_SIZE = 125;

    protected $name = 'lily-development-listing-card';
    protected static $default_properties = [
        'image' => false,
        'logo' => false,
        'title' => '',
        'bio' => '',
        'url' => ''
    ];

    public function __construct($image, $logo, string $title, string $bio)
    {
        parent::__construct([
            'image' => $image,
            'logo' => $logo,
            'title' => $title,
            'bio' => $bio
        ]);
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_WIDTH);
        }
        if ($this->logo instanceof \WP_Image) {
            $this->logo->width(static::LOGO_SIZE)->height(static::LOGO_SIZE);
        }
    }

    public function setUrl(string $url)
    {
        $this->link_url = $url;
        return $this;
    }

    public static function createFromListing(LilyDevelopmentListing $Listing)
    {
        $View = new static($Listing->featuredImage(), $Listing->logoImage(), $Listing->title(), $Listing->post()->post_excerpt);
        return $View;
    }
}
