<?php

namespace ChildTheme\Components\InstagramFeed;

use Backstage\VcLibrary\Support\Component;

/**
 * Class InstagramFeed
 * @package ChildTheme\Components\InstagramFeed
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class InstagramFeed extends Component
{
    const NAME = 'Instagram Feed';
    const TAG = 'artemis-instagram-feed';

    protected $component_config = [
        'description' => 'Create a card from a listing.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Total Posts',
                'param_name' => 'num',
                'value' => '',
                'description' => 'Set how many posts to return',
                'admin_label' => true
            ],
            [
                'type' => 'textfield',
                'heading' => 'Columns',
                'param_name' => 'cols',
                'description' => 'Set the number of columns to display your posts in.'
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        return do_shortcode(sprintf('[instagram-feed num=%s cols=%s]', $atts['num'] ?: 10, $atts['cols'] ?: 10));
    }
}
