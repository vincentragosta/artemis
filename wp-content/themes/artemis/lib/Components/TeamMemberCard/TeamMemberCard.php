<?php

namespace ChildTheme\Components\TeamMemberCard;

use Backstage\VcLibrary\Support\Component;
use ChildTheme\TeamMember\TeamMember;
use ChildTheme\TeamMember\TeamMemberRepository;

/**
 * Class TeamMemberCard
 * @package ChildTheme\Components\TeamMemberCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class TeamMemberCard extends Component
{
    const NAME = 'Team Member Card';
    const TAG = 'team_member_card';
    const VIEW = TeamMemberCardView::class;

    protected $component_config = [
        'description' => 'Create a card from a team member.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'team_members' => [
                'type' => 'dropdown',
                'heading' => 'Team Members',
                'param_name' => 'team_member_id',
                'value' => '',
                'description' => 'Select a Team Member',
                'admin_label' => true
            ]
        ]
    ];

    protected function setupConfig()
    {
        parent::setupConfig();
        $this->setTeamMembers();
    }

    protected function setTeamMembers()
    {
        $options['-- Select Team Member --'] = '';
        $TeamMemberRepository = new TeamMemberRepository();
        $team_members = $TeamMemberRepository->findAll();
        foreach ($team_members as $TeamMember) {
            /** @var TeamMember $TeamMember */
            $options[$TeamMember->post()->post_title] = $TeamMember->ID;
        }
        $this->component_config['params']['team_members']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        /* @var TeamMemberCardView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(new TeamMember($atts['team_member_id']));
    }
}
