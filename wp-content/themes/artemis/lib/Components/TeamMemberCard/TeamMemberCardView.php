<?php

namespace ChildTheme\Components\TeamMemberCard;

use Backstage\SetDesign\Modal\ModalView;
use Backstage\View\Component;
use ChildTheme\TeamMember\TeamMember;
use \WP_Image;

/**
 * Class TeamMemberCardView
 * @package ChildTheme\Components\TeamMemberCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property WP_Image $image
 * @property WP_Image $hover_image
 * @property string $role
 * @property string $title
 * @property array $social_icons
 * @property string $content
 */
class TeamMemberCardView extends Component
{
    const IMAGE_WIDTH = 768;
    const IMAGE_HEIGHT = 1152;
    const MODAL_ID = 'team-member-card-modal';

    protected $name = 'team-member-card';
    protected static  $default_properties = [
        'image' => null,
        'hover_image' => null,
        'title' => '',
        'role' => '',
        'social_icons' => [],
        'content' => ''
    ];

    public function __construct(TeamMember $TeamMember)
    {
        ModalView::load(static::MODAL_ID, 'box');
        parent::__construct([
            'image' => $TeamMember->featuredImage(),
            'hover_image' => $TeamMember->secondary_image,
            'title' => $TeamMember->title(),
            'role' => $TeamMember->role,
            'phone' => $TeamMember->phone,
            'email' => $TeamMember->email,
            'social_icons' => $TeamMember->social_icons,
            'content' => $TeamMember->content(false)
        ]);
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
        if ($this->hover_image instanceof \WP_Image) {
            $this->hover_image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
    }
}
