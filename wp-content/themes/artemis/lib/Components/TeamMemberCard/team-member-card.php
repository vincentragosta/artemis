<?php
/**
 * Expected:
 * @var WP_Image $image
 * @var WP_Image $hover_image
 * @var string $role
 * @var string $phone
 * @var string $email
 * @var string $title
 * @var array $social_icons
 * @var string content
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\SetDesign\Icon\IconView;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use Backstage\Util;
use ChildTheme\Components\TeamMemberCard\TeamMemberCardView;

if (!$image instanceof WP_Image || empty($title)) {
    return '';
}

$element_attributes['class'] = 'js-modal-builder';
$element_attributes['data-modal-target'] = '#' . TeamMemberCardView::MODAL_ID;
?>

<div <?= Util::componentAttributes('team-member-card', $class_modifiers, $element_attributes); ?>>
    <div class="team-member-card__images">
        <?= $image->css_class('team-member-card__image'); ?>
        <?php if ($hover_image instanceof WP_Image): ?>
            <?= $hover_image->css_class('team-member-card__hover-image'); ?>
        <?php endif; ?>
    </div>
    <div class="team-member-card__content-container text--center">
        <h3 class="team-member-card__heading heading heading--default"><?= $title; ?></h3>
        <?php if ($role): ?>
            <p class="team-member-card__role"><?= $role; ?></p>
        <?php endif; ?>
        <?php if (!empty($social_icons)): ?>
            <ul class="team-member-card__social-icons list list--inline">
                <?php foreach($social_icons as $SocialIcon): /* @var SocialIcon $SocialIcon */ ?>
                    <li class="team-member-card__list-item">
                        <a href="<?= $SocialIcon->getUrl(); ?>" class="link-icon--<?= $SocialIcon->getName(); ?>" target="<?= $SocialIcon->getTarget(); ?>">
                            <?= new IconView(['icon_name' => $SocialIcon->getName()]); ?>
                        </a>
                    </li>
                <?php endforeach; ?>
            </ul>
        <?php endif; ?>
        <?php if ($phone): ?>
            <p class="team-member-card__phone"><a href="tel:<?= $phone; ?>"><?= $phone; ?></a></p>
        <?php endif; ?>
        <?php if ($email): ?>
            <p class="team-member-card__email"><a href="mailto:<?= $email ?>"><?= $email ?></a></p>
        <?php endif; ?>
        <?php if ($content): ?>
            <div class="team-member-card__content">
                <?= $content; ?>
            </div>
        <?php endif; ?>
    </div>
</div>
