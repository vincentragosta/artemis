<?php

namespace ChildTheme\Components\StyledHeading;

use Backstage\VcLibrary\Support\Component;
use Backstage\View\Element;

/**
 * Class StyledHeading
 * @package ChildTheme\Components\StyledHeading
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class StyledHeading extends Component
{
    const NAME = 'Styled Heading';
    const TAG = 'styled_heading';

    protected $component_config = [
        'description' => 'Create a heading.',
        'icon' => 'icon-wpb-atm',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'textfield',
                'heading' => 'Styled Text',
                'param_name' => 'styled_content',
                'description' => 'Set the styled heading text.',
                'group' => 'Styled Text',
                'admin_label' => true
            ],
            [
                'type' => 'textfield',
                'heading' => 'Text',
                'param_name' => 'content',
                'description' => 'Set the heading text.',
                'group' => 'General',
                'admin_label' => true
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Tag',
                'param_name' => 'tag',
                'value' => [
                    'H2' => 'h2',
                    'H3' => 'h3',
                    'H4' => 'h4',
                    'H1' => 'h1'
                ],
                'description' => 'Set the heading tag [H1 -> H4]. This is for accessibility purposes only, this will not affect appearance. H1 should only be used once at the top of the page (if page title is not automatically added).',
                'group' => 'General'
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Size',
                'param_name' => 'size',
                'description' => 'Set the heading size.',
                'value' => [
                    'Medium' => 'heading--medium',
                    'Large' => 'heading--large',
                ],
                'group' => 'General'
            ],
            [
                'type' => 'dropdown',
                'heading' => 'Styled Size',
                'param_name' => 'styled_size',
                'description' => 'Set the styled heading size.',
                'value' => [
                    'Default' => 'heading--default',
                    'Medium' => 'heading--medium',
                ],
                'group' => 'Styled Text'
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        return !empty($styled_content = $atts['styled_content']) ? sprintf(
            '<div class="styled-heading">%s %s</div>',
            Element::create('strong', $styled_content, ['class' => 'heading heading--styled ' . $atts['styled_size']]),
            Element::create($atts['tag'], $atts['content'], ['class' => 'heading ' . $atts['size']])
        ) : sprintf('<div class="styled-heading">%s</div>', Element::create($atts['tag'], $atts['content'], ['class' => 'heading ' . $atts['size']]));
    }
}
