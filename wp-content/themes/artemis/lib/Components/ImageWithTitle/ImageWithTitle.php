<?php

namespace ChildTheme\Components\ImageWithTitle;

use Backstage\VcLibrary\Support\Component;

/**
 * Class ImageWithTitle
 * @package ChildTheme\Components\ImageWithTitle
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ImageWithTitle extends Component
{
    const NAME = 'Image With Title';
    const TAG = 'image_with_title';
    const VIEW = ImageWithTitleView::class;

    protected $component_config = [
        'description' => 'Create an image with a centered title',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'attach_image',
                'heading' => 'Image',
                'param_name' => 'image_id',
                'description' => 'Select an Image'
            ],
            [
                'type' => 'vc_link',
                'heading' => 'Link',
                'param_name' => 'link',
                'description' => 'Set the link',
                'admin_label' => true
            ]
        ]
    ];

    protected function createView(array $atts)
    {
        /* @var ImageWithTitleView $ViewClass */
        $ViewClass = static::VIEW;
        return new $ViewClass(\WP_Image::get_by_attachment_id($atts['image_id']), vc_build_link($atts['link']));
    }
}
