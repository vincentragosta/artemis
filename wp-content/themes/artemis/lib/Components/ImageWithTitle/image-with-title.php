<?php
/**
 * Expected:
 * @var \WP_Image $image
 * @var array $link
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

if (empty($image) || !isset($link['url'])) {
    return;
}
?>

<div <?= Util::componentAttributes('image-with-title', $class_modifiers, $element_attributes); ?>>
    <a class="image-with-title__link" href="<?= $link['url']; ?>" target="<?= $link['target']; ?>">
        <?= $image->css_class('image-with-title__image'); ?>
        <strong class="image-with-title__title"><?= $link['title']; ?></strong>
    </a>
</div>
