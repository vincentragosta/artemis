<?php

namespace ChildTheme\Components\ImageWithTitle;

use Backstage\View\Component;
use Backstage\View\Link;

/**
 * Class ImageWithTitleView
 * @package ChildTheme\Components\ImageWithTitle
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Image $image
 * @property string $title
 * @property array $link
 */
class ImageWithTitleView extends Component
{
    const IMAGE_WIDTH = 768;
    const IMAGE_HEIGHT = 474;

    protected $name = 'image-with-title';
    protected static $default_properties = [
        'image' => false,
        'link' => []
    ];

    public function __construct(\WP_Image $image, array $link)
    {
        parent::__construct(compact('image', 'link'));
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
    }
}
