<?php

namespace ChildTheme\Components\MortgageCalculator;

use Backstage\VcLibrary\Support\Component;

/**
 * Class MortgageCalculator
 * @package ChildTheme\Components\MortgageCalculator
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class MortgageCalculator extends Component
{
    const NAME = 'Mortgage Calculator';
    const TAG = 'mortgage_calculator';
    const VIEW = MortgageCalculatorView::class;
    const REPLACE = '#';

    protected $component_config = [
        'description' => 'Create a mortgage calculator.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            [
                'type' => 'colorpicker',
                'heading' => 'Text color',
                'param_name' => 'text_color',
                'description' => 'Select a text color',
                'admin_label' => true
            ],
            [
                'type' => 'colorpicker',
                'heading' => 'Background color',
                'param_name' => 'background_color',
                'description' => 'Select a background color',
                'admin_label' => true
            ]
        ]
    ];

    public function createView(array $atts)
    {
        $ViewClass = static::VIEW;
        return new $ViewClass(
            str_replace(static::REPLACE, '', $atts['text_color']),
            str_replace(static::REPLACE, '', $atts['background_color'])
        );
    }
}
