<?php
/**
 * Expected:
 * @var string $text_color
 * @var string $background_color
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;

?>

<div <?= Util::componentAttributes('mortgage-calculator', $class_modifiers, $element_attributes); ?>>
    <iframe class="mortgage-calculator__tablet" src ="https://www.mortgagecalculator.net/embeddable/v2/?size=2&textColor=<?= $text_color; ?>&backgroundColor=<?= $background_color; ?>" width="100%" frameborder=2 scrolling=no height=330></iframe>
    <iframe class="mortgage-calculator__phablet" src ="https://www.mortgagecalculator.net/embeddable/v2/?size=3&textColor=<?= $text_color; ?>&backgroundColor=<?= $background_color; ?>" width="100%" frameborder=2 scrolling=no height=330></iframe>
    <iframe class="mortgage-calculator__mobile" src ="https://www.mortgagecalculator.net/embeddable/v2/?size=5&textColor=<?= $text_color; ?>&backgroundColor=<?= $background_color; ?>" width="100%" frameborder=2 scrolling=no height=330></iframe>
</div>
