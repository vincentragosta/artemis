<?php

namespace ChildTheme\Components\MortgageCalculator;

use Backstage\View\Component;

/**
 * Class MortgageCalculatorView
 * @package ChildTheme\Components\MortgageCalculator
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $text_color
 * @property string $background_color
 */
class MortgageCalculatorView extends Component
{
    const TEXT_COLOR = 'FFFFFF';
    const BACKGROUND_COLOR = '222222';

    protected $name = 'mortgage-calculator';
    protected static $default_properties = [
        'text_color' => '',
        'background_color' => ''
    ];

    public function __construct(string $text_color = '', string $background_color = '')
    {
        parent::__construct(compact('text_color', 'background_color'));
        $this->text_color = !empty($this->text_color) ? $this->text_color : static::TEXT_COLOR;
        $this->background_color = !empty($this->background_color) ? $this->background_color : static::BACKGROUND_COLOR;
    }
}
