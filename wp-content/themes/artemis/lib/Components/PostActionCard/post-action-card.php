<?php
/**
 * Expected:
 * @var string $url
 * @var WP_Image $image
 * @var string $title
 * @var string $tagline
 * @var string $content
 * @var array $social_sharer
 * @var DateTime $date
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use Backstage\View\Link;
use Backstage\Support\DateTime;

$element_attributes['class'] = 'responsive-component';

if (empty($url) || empty($title)) {
    return '';
}
?>

<div <?= Util::componentAttributes('post-action-card', $class_modifiers, $element_attributes); ?>>
    <?php if ($image instanceof WP_Image): ?>
        <div class="post-action-card__image-container">
            <?= new Link($url, $image->css_class('post-action-card__image')); ?>
        </div>
    <?php endif; ?>
    <div class="post-action-card__content-container">
        <h3 class="post-action-card__heading heading heading--default"><?= new Link($url, $title); ?></h3>
        <?php if (!empty($tagline)): ?>
            <p class="post-action-card__tagline"><?= $tagline; ?></p>
        <?php endif; ?>
        <?php if (!empty($content)): ?>
            <div class="post-action-card__content">
                <?= $content; ?>
            </div>
        <?php endif; ?>
        <div class="post-action-card__sharer-container">
            <div class="post-action-card__date-comments">
                <?php if (!empty($date_comments)): ?>
                    <?= $date_comments; ?>
                <?php endif; ?>
            </div>
            <?php if (!empty($social_sharer)): ?>
                <div class="post-action-card__sharer">
                    <ul class="list--inline">
                        <?php foreach ($social_sharer as $SocialSharer): ?>
                            <li><?= $SocialSharer; ?></li>
                        <?php endforeach; ?>
                    </ul>
                </div>
            <?php endif; ?>
        </div>
    </div>
</div>
