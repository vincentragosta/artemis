<?php

namespace ChildTheme\Components\PostActionCard;

use Backstage\View\Component;
use Backstage\View\Link;
use ChildTheme\SocialSharer\SocialSharerLinks;

/**
 * Class PostActionCardView
 * @package ChildTheme\Components\PostActionCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property string $title
 * @property string $content
 * @property string $image
 * @property string $url
 * @property array $social_sharer
 * @property string $date_comments
 * @property array $class_modifiers
 * @property array $element_attributes
 */
class PostActionCardView extends Component
{
    const TRIM_WORD_COUNT = 15;
    const IMAGE_WIDTH = 768;
    const IMAGE_HEIGHT = 576;
    const DATE_FORMAT = 'd F, Y';
    const COMMENTS_LABEL = '%s comments';
    const DATE_COMMENTS_LABEL = '%s / %s';

    protected $name = 'post-action-card';
    protected static $default_properties = [
        'title' => '',
        'tagline' => '',
        'content' => '',
        'image' => false,
        'url' => '',
        'social_sharer' => [],
        'comments_number' => 0
    ];

    public function __construct(PostActionCardItem $Post)
    {
        $args = [
            'url' => $Post->permalink(),
            'title' => $Post->title(),
            'tagline' => $Post->getTagline(),
            'content' => $Post->excerpt(),
            'image' => $Post->featuredImage(),
            'date_comments' => sprintf(
                static::DATE_COMMENTS_LABEL,
                $Post->publishedDate()->format(static::DATE_FORMAT),
                new Link($Post->permalink() . '#comments', sprintf(static::COMMENTS_LABEL, $Post->getCommentsNumber()))
            ),
        ];
        if ($Post instanceof SocialSharerLinks) {
            $args['social_sharer'] = $Post->getSocialSharerLinks();
        }
        parent::__construct($args);
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
        if ($this->content) {
            $trimmed_text = wp_trim_words($this->content, static::TRIM_WORD_COUNT, '');
            if (strlen($this->content) !== strlen($trimmed_text) && $this->url) {
                $trimmed_text .= new Link($this->url, '...', ['class' => 'post-action-card__content-ellipsis']);
            }
            $this->content = $trimmed_text;
        }
    }
}
