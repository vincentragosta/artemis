<?php
/**
 * Expected:
 * @var bool $is_sold
 * @var \WP_Image $image
 * @var string $type
 * @var string $street_address
 * @var string $city
 * @var string $state
 * @var string $zip_code
 * @var string $list_price
 * @var string $beds
 * @var string $baths
 * @var array $link
 * @var array $investors_performa_link
 * @var array $class_modifiers
 * @var array $element_attributes
 */

use Backstage\Util;
use ChildTheme\Components\ListingCard\ListingCard;

if (empty($image) || empty($street_address)) {
    return;
}
?>

<div <?= Util::componentAttributes('listing-card', $class_modifiers, $element_attributes); ?>>
    <?php if (!empty($url = $link['url'])): ?>
        <a class="listing-card__link" href="<?= $url; ?>" target="<?= $link['target']; ?>" aria-label="<?= $link['title']; ?>">
    <?php endif; ?>
        <?= $image->css_class('listing-card__image'); ?>
        <div class="listing-card__content-container">
            <h3 class="listing-card__list-price heading heading--medium"><?= sprintf('%s: $%s', $is_sold ? 'Sold' : 'List', $list_price); ?></h3>
            <p class="listing-card__type heading"><?= $type; ?></p>
            <strong class="listing-card__heading heading"><?= $street_address; ?></strong>
            <p class="listing-card__city-state-zip heading"><?= $city; ?>, <?= $state; ?> <?= $zip_code; ?></p>
            <?php if (!empty($beds) || !empty($baths)): ?>
                <ul class="listing-card__bed-bath list list--inline">
                    <?php if ($beds): ?>
                        <li class="heading">Beds: <?= $beds; ?></li>
                    <?php endif; ?>
                    <?php if ($baths): ?>
                        <li class="heading">Baths: <?= $baths; ?></li>
                    <?php endif; ?>
                </ul>
            <?php endif; ?>
        </div>
    <?php if (!empty($url)): ?>
        </a>
    <?php endif; ?>
</div>
<?php if (in_array(ListingCard::INVESTORS_PERFORMA_LABEL, $class_modifiers)): ?>
    <a href="<?= $link['url']; ?>" class="button button--styled">Download Pro Forma</a>
<?php endif; ?>
