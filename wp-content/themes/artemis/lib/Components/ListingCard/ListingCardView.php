<?php

namespace ChildTheme\Components\ListingCard;

use Backstage\View\Component;
use ChildTheme\Listing\Listing;
use \WP_Image;
use \WP_Term;

/**
 * Class ListingCardView
 * @package ChildTheme\Components\ListingCard
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $type
 * @property bool $is_sold
 * @property WP_Image $image
 * @property string $street_address
 * @property string $city
 * @property string $state
 * @property string $zip_code
 * @property string $listing_price
 * @property string $beds
 * @property string $baths
 * @property array $link
 * @property bool $investors_performa
 */
class ListingCardView extends Component
{
    const IMAGE_WIDTH = 768;
    const IMAGE_HEIGHT = 474;

    protected $name = 'listing-card';
    protected static $default_properties = [
        'type' => '',
        'is_sold' => false,
        'image' => false,
        'street_address' => '',
        'city' => '',
        'state' => '',
        'zip_code' => '',
        'listing_price' => '',
        'beds' => '',
        'baths' => '',
        'link' => [],
        'investors_performa' => false
    ];

    public function __construct(Listing $Listing, array $link = [])
    {
        /** @var WP_Term $listing_type */
        parent::__construct([
            'type' => ($listing_type = $Listing->listing_type) instanceof WP_Term ? $listing_type->name : '',
            'is_sold' => $Listing->is_sold,
            'image' => $Listing->featuredImage(),
            'street_address' => $Listing->getStreetAddress(),
            'city' => $Listing->getCity(),
            'state' => $Listing->getState(),
            'zip_code' => $Listing->getZipCode(),
            'list_price' => $Listing->list_price,
            'beds' => $Listing->beds,
            'baths' => $Listing->baths,
            'link' => $link
        ]);
        if ($this->image instanceof \WP_Image) {
            $this->image->width(static::IMAGE_WIDTH)->height(static::IMAGE_HEIGHT);
        }
    }
}
