<?php

namespace ChildTheme\Components\ListingCard;

use Backstage\SetDesign\Modal\ModalView;
use Backstage\VcLibrary\Support\Component;
use ChildTheme\Listing\Listing;
use ChildTheme\Listing\ListingRepository;

/**
 * Class ListingCard
 * @package ChildTheme\Components\ListingCard
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class ListingCard extends Component
{
    const NAME = 'Listing Card';
    const TAG = 'listing_card';
    const VIEW = ListingCardView::class;
    const INVESTORS_PERFORMA_LABEL = 'investors-performa';

    protected $component_config = [
        'description' => 'Create a card from a listing.',
        'icon' => 'icon-wpb-toggle-small-expand',
        'wrapper_class' => 'clearfix',
        'is_container' => false,
        'category' => 'Content',
        'params' => [
            'listings' => [
                'type' => 'dropdown',
                'heading' => 'Listings',
                'param_name' => 'listing_id',
                'value' => '',
                'description' => 'Select a Listing',
                'admin_label' => true
            ],
            [
                'type' => 'vc_link',
                'heading' => 'Link',
                'param_name' => 'link',
                'description' => 'Set a click event on the card'
            ],
            [
                'type' => 'checkbox',
                'heading' => 'Investors Pro Forma',
                'param_name' => 'investors_performa',
                'description' => 'This will override the link URL set on the element.',
                'value' => [
                    'Enable Investors Pro Forma on selected listing?' => true
                ]
            ]
        ]
    ];

    protected function setupConfig()
    {
        parent::setupConfig();
        $this->setListings();
    }

    protected function setListings()
    {
        $options['-- Select Listing --'] = '';
        $ListingRepository = new ListingRepository();
        $listings = $ListingRepository->findAll();
        foreach ($listings as $Listing) {
            /** @var Listing $Listing */
            $options[$Listing->post()->post_title] = $Listing->ID;
        }
        $this->component_config['params']['listings']['value'] = $options;
    }

    protected function createView(array $atts)
    {
        /* @var ListingCardView $ViewClass */
        $ViewClass = static::VIEW;
        $Listing = new Listing($atts['listing_id']);
        $link = vc_build_link($atts['link']);
        if ($atts['investors_performa'] && $Listing instanceof Listing) {
            ModalView::load($this->getModalId($Listing), 'box', do_shortcode('[gravityform id="5" title="true" description="true" ajax="true" field_values="listing_id=' . $atts['listing_id'] . '"]'));
            if (isset($link['url'])) {
                $link['url'] = '#' . $this->getModalId($Listing);
                unset($link['target']);
            }
            return (new $ViewClass($Listing, $link))->classModifiers(static::INVESTORS_PERFORMA_LABEL);
        }
        return new $ViewClass($Listing, $link);
    }

    protected function getModalId(Listing $Listing)
    {
        return sprintf('%s-%s', static::INVESTORS_PERFORMA_LABEL, $Listing->post_name);
    }
}
