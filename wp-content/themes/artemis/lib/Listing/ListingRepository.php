<?php

namespace ChildTheme\Listing;

use Backstage\Repositories\PostRepository;

/**
 * Class ListingRepository
 * @package ChildTheme\Listing
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class ListingRepository extends PostRepository
{
    protected $model_class = Listing::class;
}
