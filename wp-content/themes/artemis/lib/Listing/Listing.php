<?php

namespace ChildTheme\Listing;

use Backstage\Models\PostBase;

/**
 * Class Listing
 * @package ChildTheme\Listing
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property \WP_Term $listing_type
 * @property bool $is_sold
 * @property string $list_price
 * @property string $beds
 * @property string $baths
 * @property array $address
 * @property array $investors_performa_file
 */
class Listing extends PostBase
{
    const POST_TYPE = 'listing';
    const TAXONOMY = 'listing-type';

    public function getStreetAddress()
    {
        $address = $this->field('address');
        return $address['street_address'] ?: '';
    }

    public function getCity()
    {
        $address = $this->field('address');
        return $address['city'] ?: '';
    }

    public function getState()
    {
        $address = $this->field('address');
        return $address['state'] ?: '';
    }

    public function getZipCode()
    {
        $address = $this->field('address');
        return $address['zip_code'] ?: '';
    }
}
