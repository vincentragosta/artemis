<?php

namespace ChildTheme\Sidebar;

/**
 * Class SidebarFactory
 * @package ChildTheme\Sidebar
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class SidebarFactory
{
    const DEFAULT_SIDEBAR_SETTINGS = [
        'before_widget' => '<div class="content-column content-column--widget col-md-12"><div class="content-column__inner">',
        'after_widget' => '</div></div>',
        'before_title' => '<h2 class="heading heading--default">',
        'after_title' => '</h2>'
    ];

    public function __construct(array $sidebar_names)
    {
        foreach($sidebar_names as $name) {
            $this->register($name);
        }
    }

    protected function register(string $name)
    {
        register_sidebar(
            array_merge($this->getNameSettings($name), static::DEFAULT_SIDEBAR_SETTINGS)
        );
    }

    protected function getNameSettings(string $name)
    {
        return array_merge(compact('name'), ['id' => sanitize_text_field($name)]);
    }
}
