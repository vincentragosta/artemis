<?php

namespace ChildTheme\Sidebar;

use Backstage\Models\Page;

/**
 * Class SidebarPage
 * @package ChildTheme\Sidebar
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $sidebar_name
 */
class SidebarPage extends Page {}
