<?php

namespace ChildTheme\Sidebar;

/**
 * Class SidebarController
 * @package ChildTheme\Sidebar
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class SidebarController
{
    const SIDEBARS = ['Primary Sidebar', 'Secondary Sidebar'];

    const WIDGETS_BLACKLIST = [
        'WP_Widget_Pages',
        'WP_Widget_Calendar',
        'WP_Widget_Archives',
        'WP_Widget_Links',
        'WP_Widget_Meta',
        'WP_Widget_Recent_Comments',
        'WP_Widget_RSS',
        'WP_Widget_Tag_Cloud',
        'WP_Widget_Media_Audio',
        'WP_Widget_Media_Gallery',
        'WP_Widget_Custom_HTML',
        'WP_Nav_Menu_Widget',
        'Twenty_Eleven_Ephemera_Widget'
    ];

    public function __construct()
    {
        add_action('widgets_init', new SidebarFactory(static::SIDEBARS));
        add_action('widgets_init', [$this, 'removeWidgets']);
    }

    public function removeWidgets()
    {
        if (empty($widgets = static::WIDGETS_BLACKLIST)) {
            return [];
        }
        return array_walk($widgets, 'unregister_widget');
    }
}
