<?php

namespace ChildTheme\Sidebar\Widgets;

use Backstage\Producers\Video\VideoPost;
use Backstage\Producers\Video\VideoRepository;
use Backstage\SetDesign\Video\VideoThumb;

/**
 * Class Video
 * @package ChildTheme\Sidebar\Widgets
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class Video extends \WP_Widget
{
    const OPTIONS = [
        'classname' => 'widget-video',
        'description' => 'Display a video.'
    ];

    public function __construct()
    {
        parent::__construct('video', __('Video'), static::OPTIONS);
    }

    public function register()
    {
        add_action('widgets_init', function () {
            register_widget(static::class);
        });
    }

    public function widget($args, $instance)
    {
        if (!($VideoPost = new VideoPost($instance['video_id'])) instanceof VideoPost) {
            return;
        }
        $title = (!empty($instance['title'])) ? $instance['title'] : __('Recent Posts');
        echo $args['before_widget'];
        if ($title) {
            echo $args['before_title'] . $title . $args['after_title'];
        }
        echo VideoThumb::createFromVideoPost($VideoPost);
        echo $args['after_widget'];
    }

    public function update($new_instance, $old_instance)
    {
        $instance = $old_instance;
        $instance['title'] = sanitize_text_field($new_instance['title']);
        $instance['video_id'] = sanitize_text_field($new_instance['video_id']);
        return $instance;
    }

    public function form($instance)
    {
        $VideoRepository = new VideoRepository();
        $videos = $VideoRepository->findAll();
        error_log(var_export($videos, true));
        $title = isset($instance['title']) ? esc_attr($instance['title']) : '';
        $video_id = isset($instance['video_id']) ? esc_attr($instance['video_id']) : ''; ?>
        <p>
            <label for="<?= $this->get_field_id('title'); ?>"><?php _e('Title:'); ?></label>
            <input class="widefat" id="<?= $this->get_field_id('title'); ?>" name="<?= $this->get_field_name('title'); ?>" type="text" value="<?= $title; ?>" />
        </p>
        <p>
            <label for="<?= $this->get_field_id('video_id'); ?>"><?php _e('Video:'); ?></label>
            <select class="widefat" id="<?= $this->get_field_id('video_id'); ?>" name="<?= $this->get_field_name('video_id'); ?>">
                <option value="">Select Video</option>
                <?php foreach($videos as $VideoPost): ?>
                    <option value="<?= $VideoPost->ID; ?>" <?= $video_id == $VideoPost->ID ? 'selected' : ''; ?>><?= $VideoPost->post()->post_title; ?></option>
                <?php endforeach; ?>
            </select>
        </p><?php
    }
}
