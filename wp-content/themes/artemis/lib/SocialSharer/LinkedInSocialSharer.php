<?php

namespace ChildTheme\SocialSharer;

/**
 * Class LinkedInSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class LinkedInSocialSharer extends SocialSharer
{
    const SHARER = 'https://www.linkedin.com/sharing/share-offsite/?url=%s';
    const ICON = 'linkedin';

    protected function generateSharerUrl(string $url, string $text = null)
    {
        if (empty($url)) {
            return '';
        }
        return sprintf(static::SHARER, $url);
    }
}
