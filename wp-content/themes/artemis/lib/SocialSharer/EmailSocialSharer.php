<?php

namespace ChildTheme\SocialSharer;

/**
 * Class EmailSocialSharer
 * @package ChildTheme\SocialSharer
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class EmailSocialSharer extends SocialSharer
{
    const SHARER = 'mailto:%s?subject=%s&body=%s';
    const ICON = 'email';

    protected function generateSharerUrl(string $url, string $text = null)
    {
        if (empty($url)) {
            return '';
        }
        return sprintf(static::SHARER, $url, $text);
    }
}
