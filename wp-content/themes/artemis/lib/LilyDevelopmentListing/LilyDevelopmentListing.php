<?php

namespace ChildTheme\LilyDevelopmentListing;

use Backstage\Models\PostBase;

/**
 * Class LilyDevelopmentListing
 * @package ChildTheme\LilyDevelopmentListing
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @property string $logo_id
 * @property \WP_Term $lily_development_listing_type
 * @property string $list_price
 * @property string $beds
 * @property string $baths
 * @property string $square_footage
 * @property array $listing_agent
 * @property string $landscape_image_id
 * @property array $images
 */
class LilyDevelopmentListing extends PostBase
{
    const POST_TYPE = 'lily-development';
    const TAXONOMY = 'lily-development-listing-type';

    public function logoImage()
    {
        return \WP_Image::get_by_attachment_id($this->field('logo_id'));
    }

    public function landscapeImage()
    {
        return \WP_Image::get_by_attachment_id($this->field('landscape_image_id'));
    }

    public function listingAgentImage()
    {
        $agent = $this->field('listing_agent');
        return !empty($agent) ? \WP_Image::get_by_attachment_id($agent['image_id']) : '';
    }

    public function listingAgentName()
    {
        $agent = $this->field('listing_agent');
        return !empty($agent) ? $agent['name'] : '';
    }

    public function listingAgentPhone()
    {
        $agent = $this->field('listing_agent');
        return !empty($agent) ? $agent['phone'] : '';
    }

    public function listingAgentEmail()
    {
        $agent = $this->field('listing_agent');
        return !empty($agent) ? $agent['email'] : '';
    }

    public function listingAgentBio()
    {
        $agent = $this->field('listing_agent');
        return !empty($agent) ? $agent['bio'] : '';
    }
}
