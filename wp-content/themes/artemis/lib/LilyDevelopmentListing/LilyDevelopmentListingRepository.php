<?php

namespace ChildTheme\LilyDevelopmentListing;

use Backstage\Repositories\PostRepository;

/**
 * Class LilyDevelopmentListingRepository
 * @package ChildTheme\LilyDevelopmentListing
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class LilyDevelopmentListingRepository extends PostRepository
{
    protected $model_class = LilyDevelopmentListing::class;
}
