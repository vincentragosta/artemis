<?php

namespace ChildTheme;

use ChildTheme\BlogPost\BlogPostController;
use ChildTheme\Controller;
use ChildTheme\Components;
use ChildTheme\Search\SearchController;
use ChildTheme\Sidebar\SidebarController;
use Orchestrator\Theme as ThemeBase;

/**
 * Class Theme
 *
 * Configure settings by overriding parent class constants
 *
 * @package Theme
 */
class Theme extends ThemeBase
{
    const RENAME_DEFAULT_POST_TYPE = 'Blog Post';

    const EXTENSIONS = [
        Controller\VcLibraryController::class,
        BlogPostController::class,
        SidebarController::class,
        Controller\GravityFormsController::class,
        Controller\CommentsController::class,
        Controller\FollowUpBossPixelController::class,
        SearchController::class,
        Components\PostActionCard\PostActionCard::class,
        Components\TeamMemberCard\TeamMemberCard::class,
        Components\ImageWithTitle\ImageWithTitle::class,
        Components\ListingCard\ListingCard::class,
        Components\MortgageCalculator\MortgageCalculator::class,
        Components\StyledHeading\StyledHeading::class,
        Components\SearchForm\SearchForm::class,
        Components\InstagramFeed\InstagramFeed::class,
        Components\LilyDevelopmentListingCard\LilyDevelopmentListingCard::class
    ];

    const PLATFORM_THEME_SUPPORT = [
        'set-design/nav-menu',
        'design-producer',
        'video-producer',
        'media-gallery-producer',
        'preview-producer'
    ];

    /**
     * Add theme-specific hooks
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Add theme-specific style and script enqueues
     */
    public function assets()
    {
        parent::assets();
        wp_enqueue_script('masonryjs', 'https://unpkg.com/masonry-layout@4/dist/masonry.pkgd.min.js', ['jquery'], '1.0', true);
    }
}
