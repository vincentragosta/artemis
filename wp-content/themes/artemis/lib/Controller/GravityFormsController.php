<?php

namespace ChildTheme\Controller;

use ChildTheme\Listing\Listing;
use ChildTheme\Options\GlobalOptions;

/**
 * Class GravityFormsController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class GravityFormsController
{
    const CONTACT_FORM_ID = 3;
    const CONSULATATION_FORM_ID = 4;
    const INVESTORS_PERFORMA_FORM_ID = 5;
    const OPEN_HOUSE_CHECKIN_FORM_ID = 7;
    const LISTING_COLUMN_LABEL = '<a href="%s">%s</a>';
    const EMAIL_MESSAGE = 'Hello Artemis,

    A new inquiry has been submitted:
    First Name: %s
    Last Name: %s
    Email: %s
    Phone: %s
    Additional Comments: %s';
    const OPEN_HOUSE_EMAIL_MESSAGE = 'Hello Artemis,

    Open House Location: %s
    Full Name: %s
    Email: %s
    Phone: %s
    Current Address: %s
    Are you interested in: %s
    Are you working with a realtor: %s
    If you are a seller are you interested in scheduling a consultation to list your home with us: %s
    If you are a buyer or investor, what is your budget (range is acceptable): %s
    What is your desired neighborhood: %s
    What is your search criteria: %s';

    public function __construct()
    {
        add_action('gform_after_submission_' . static::CONTACT_FORM_ID, [$this, 'sendEmailOnSubmission'], 10, 2);
        add_action('gform_after_submission_' . static::CONSULATATION_FORM_ID, [$this, 'sendConsultationEmail'], 10, 2);
        add_action('gform_after_submission_' . static::OPEN_HOUSE_CHECKIN_FORM_ID, [$this, 'sendOpenHouseCheckInEmail'], 10, 2);
        add_filter('gform_entries_column_filter', [$this, 'listingColumn'], 10, 3);

        add_filter('gform_confirmation', function($confirmation, $form, $entry) {
            if ($form['id'] == static::INVESTORS_PERFORMA_FORM_ID) {
                $Listing = new Listing($entry['5']);
                if (!$Listing instanceof Listing) {
                    return $confirmation;
                }
                if (empty($file = $Listing->investors_performa_file)) {
                    return $confirmation;
                }
                $confirmation = sprintf('Thank you for your submission. Please click the button below to download your listing pro forma.<br /><br /><a href="%s" class="button button--styled" download>Download Pro Forma</a>', $file['url']);
            }
            return $confirmation;
        }, 10, 3);


        add_action('gform_after_submission_' . static::INVESTORS_PERFORMA_FORM_ID, function ($entry, $form) {
            if (empty($entry['id'])) {
                return;
            }
            wp_mail(
                GlobalOptions::contactEmail(),
                'Artemis: Investors Pro Forma Contact',
                'You have a new investors Pro Forma entry! Please check the "Investors Pro Forma" form records.'
            );
        }, 10, 2);
    }

    public function sendEmailOnSubmission($entry, $form)
    {
        if (empty($entry['id'])) {
            return;
        }
        wp_mail(
            GlobalOptions::contactEmail(),
            'Artemis: Contact Inquiry',
            sprintf(static::EMAIL_MESSAGE, $entry['1.3'], $entry['2.6'], $entry['3'], $entry['4'], $entry['5'])
        );
    }

    public function sendConsultationEmail($entry, $form)
    {
        if (empty($entry['id'])) {
            return;
        }
        wp_mail(
            GlobalOptions::contactEmail(),
            'Artemis: Schedule A Consultation',
            'You have a new consultation! Please check the "Schedule a Consultation" form records.'
        );
    }

    public function sendOpenHouseCheckinEmail($entry, $form)
    {
        if (empty($entry['id'])) {
            return;
        }
        wp_mail(
            GlobalOptions::contactEmail(),
            'Artemis: Open House CheckIn',
            sprintf(static::OPEN_HOUSE_EMAIL_MESSAGE, $entry['1'], $entry['2.3'], $entry['3'], $entry['4'], $entry['5.1'], $entry['6'], $entry['7'], $entry['8'], $entry['10'], $entry['11'], $entry['12'])
        );
    }

    public function listingColumn($value, $form_id, $field_id)
    {
        if ($form_id == static::INVESTORS_PERFORMA_FORM_ID && $field_id == 5) {
            return !empty($value) ? sprintf(static::LISTING_COLUMN_LABEL, get_edit_post_link($value), (new Listing($value))->title()) : $value;
        }
        return $value;
    }
}
