<?php

namespace ChildTheme\Controller;

/**
 * Class CommentsController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class CommentsController
{
    public function __construct()
    {
        add_filter('comment_form_default_fields', [$this, 'removeWebsiteField']);
    }

    public function removeWebsiteField($fields)
    {
        if (isset($fields['url'])) {
            unset($fields['url']);
        }
        if (isset($fields['email'])) {
            unset($fields['email']);
        }
        return $fields;
    }
}
