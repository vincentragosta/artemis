<?php

namespace ChildTheme\Controller;

/**
 * Class VcLibraryController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class VcLibraryController
{
    const STYLED_LABEL = 'styled';

    protected static $additional_background_colors = [
        'White' => 'inverted',
        'Primary' => 'primary',
        'Black' => 'black',
        'Darker' => 'darker'
    ];

    protected static $button_styles = [
        'Select Style' => '',
        'Styled' => self::STYLED_LABEL,
        'Transparent' => 'transparent'
    ];

    protected static $component_blacklist = [
        'content_card',
        'content_block',
        'callout',
        'sub-navigation',
        'ticket_calendar',
        'talent_card',
        'media_carousel',
        'image_carousel'
    ];

    public function __construct()
    {
        add_filter('backstage/vc-library/background-colors/vc_section', [$this, 'addBackgroundColors']);
        add_filter('backstage/vc-library/button/styles', [$this, 'buttonStyles']);
        add_filter('backstage/vc-library/button/title', [$this, 'styledButton'], 10, 2);
        add_filter('backstage/vc-library/blacklist', [$this, 'blacklist']);
    }

    public function addBackgroundColors($colors)
    {
        return array_merge($colors, static::$additional_background_colors);
    }

    public function buttonStyles($styles)
    {
        return static::$button_styles;
    }

    public function styledButton($title, $style)
    {
        if ($style !== static::STYLED_LABEL) {
            return $title;
        }
        return sprintf('%s &raquo;', $title);
    }

    public function blacklist($components)
    {
        return array_merge($components, static::$component_blacklist);
    }
}
