<?php

namespace ChildTheme\Controller;

/**
 * Class FollowUpBossPixelController
 * @package ChildTheme\Controller
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 */
class FollowUpBossPixelController
{
    public function __construct()
    {
        add_action('wp_head', function() { ?>
            <!-- begin Widget Tracker Code -->
            <script>
                (function(w,i,d,g,e,t){w["WidgetTrackerObject"]=g;(w[g]=w[g]||function()
                {(w[g].q=w[g].q||[]).push(arguments);}),(w[g].ds=1*new Date());(e="script"),
                    (t=d.createElement(e)),(e=d.getElementsByTagName(e)[0]);t.async=1;t.src=i;
                    e.parentNode.insertBefore(t,e);})
                (window,"https://widgetbe.com/agent",document,"widgetTracker");
                window.widgetTracker("create", "WT-SFCPMWNB");
                window.widgetTracker("send", "pageview");
            </script>
            <!-- end Widget Tracker Code
            -->
        <?php });
    }
}
