<?php

namespace ChildTheme\Options;

use Backstage\Models\OptionsBase;
use Backstage\Models\Page;
use Backstage\SetDesign\SocialIcons\SocialIcon;
use WP_Image;

/**
 * Class GlobalOptions
 * @package Orchestrator\Options
 *
 * @method static WP_Image headerBrandImage()
 * @method static WP_Image additionalBrandImage()
 * @method static socialIcons()
 * @method static contactEmail()
 * @method static contactPhone()
 * @method static blogPage()
 */
class GlobalOptions extends OptionsBase
{
    protected $default_values = [
        'header__brand_image' => null,
        'additional_brand_image' => null,
        'social_icons' => [],
        'contact_email' => '',
        'contact_phone' => '',
        'blog_page' => null
    ];

    protected function getHeaderBrandImage()
    {
        $header_image = $this->get('header__brand_image');
        return WP_Image::get_by_attachment_id($header_image);
    }

    protected function getAdditionalBrandImage()
    {
        $image = $this->get('additional_brand_image');
        return WP_Image::get_by_attachment_id($image);
    }

    protected function getSocialIcons()
    {
        return array_map(function($row) {
            $label = isset($row['link']['title']) ? $row['link']['title'] : ucfirst($row['icon']);
            return new SocialIcon($row['icon'], $row['link']['url'] ?: '', $label, []);
        }, $this->get('social_icons'));
    }

    protected function getBlogPage()
    {
        $page = $this->get('blog_page');
        return new Page($page);
    }
}
