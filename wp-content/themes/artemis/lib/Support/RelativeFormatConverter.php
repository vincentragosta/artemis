<?php

namespace ChildTheme\Support;

/**
 * Class RelativeFormatConverter
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vincentpasqualeragosta@gmail.com>
 * @version 1.0
 *
 * @source https://www.geeksforgeeks.org/how-to-convert-timestamp-to-time-ago-in-php/
 */
class RelativeFormatConverter
{
    protected $seconds;
    protected $minutes;
    protected $hours;
    protected $days;
    protected $weeks;
    protected $months;
    protected $years;

    public function __construct(string $time)
    {
        // Calculate difference between current
        // time and given timestamp in seconds
        $diff = time() - $time;

        // Time difference in seconds
        $this->seconds = $diff;

        // Convert time difference in minutes
        $this->minutes = round($diff / 60);

        // Convert time difference in hours
        $this->hours = round($diff / 3600);

        // Convert time difference in days
        $this->days = round($diff / 86400);

        // Convert time difference in weeks
        $this->weeks = round($diff / 604800);

        // Convert time difference in months
        $this->months = round($diff / 2600640);

        // Convert time difference in years
        $this->years = round($diff / 31207680);
    }

    public function getRelativeTime()
    {
        // Check for seconds
        if ($this->seconds <= 60) {
            return sprintf('%s seconds ago', $this->seconds);
            // Check for minutes
        } else if ($this->minutes <= 60) {
            if ($this->minutes == 1) {
                return 'one minute ago';
            } else {
                return sprintf('%s minutes ago', $this->minutes);
            }
            // Check for hours
        } else if ($this->hours <= 24) {
            if ($this->hours == 1) {
                return 'an hour ago';
            } else {
                return sprintf('%s hours ago', $this->hours);
            }
            // Check for days
        } else if ($this->days <= 7) {
            if ($this->days == 1) {
                return 'Yesterday';
            } else {
                return sprintf('%s days ago', $this->days);
            }
            // Check for weeks
        } else if ($this->weeks <= 4.3) {
            if ($this->weeks == 1) {
                return 'a week ago';
            } else {
                return sprintf('%s weeks ago', $this->weeks);
            }
            // Check for months
        } else if ($this->months <= 12) {
            if ($this->months == 1) {
                return 'a month ago';
            } else {
                return sprintf('%s months ago', $this->months);
            }
            // Check for years
        } else {
            if ($this->years == 1) {
                return 'one year ago';
            } else {
                return sprintf('%s years ago', $this->years);
            }
        }
    }
}
