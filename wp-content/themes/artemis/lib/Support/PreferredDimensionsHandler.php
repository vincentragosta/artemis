<?php

namespace ChildTheme\Support;

use ChildTheme\BlogPost\BlogPost;

/**
 * Class PreferredDimensionsHandler
 * @package ChildTheme\Support
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class PreferredDimensionsHandler
{
    const DIMENSIONS_LABEL = '<p>Preferred Dimensions: %d x %d</p> %s';
    const DETAIL_WIDTH = 2000;
    const DETAIL_HEIGHT = 800;
    const POST_TYPES = [
        BlogPost::class
    ];

    public function __construct()
    {
        add_filter('admin_post_thumbnail_html', function ($content, $post_id) {
            $post = get_post($post_id);
            if (!in_array($post->post_type, static::POST_TYPES)) {
                return $content;
            }
            return $content = sprintf(static::DIMENSIONS_LABEL, static::DETAIL_WIDTH, static::DETAIL_HEIGHT, $content);
        }, 10, 2);
    }
}
