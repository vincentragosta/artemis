<?php

namespace ChildTheme\BlogPost;

use Backstage\Producers\RelatedContent\RelatedContentRepository;

/**
 * Class BlogPostRepository
 * @package ChildTheme\BlogPost
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 */
class BlogPostRepository extends RelatedContentRepository
{
    protected $model_class = BlogPost::class;
    protected $related_model_class = BlogPost::class;
}
