<?php

namespace ChildTheme\BlogPost;

use ChildTheme\Components\PostActionCard\PostActionCardItem;
use ChildTheme\SocialSharer\FacebookSocialSharer;
use ChildTheme\SocialSharer\LinkedInSocialSharer;
use ChildTheme\SocialSharer\PinterestSocialSharer;
use ChildTheme\SocialSharer\SocialSharerLinks;
use ChildTheme\SocialSharer\TwitterSocialSharer;
use ChildTheme\RelatedContent\RelatedContentPost;
use WP_Image;

/**
 * Class BlogPost
 * @package ChildTheme\BlogPost
 * @author Vincent Ragosta <vragosta@situationinteractive.com>
 * @version 1.0
 *
 * @property WP_Image $landscape_image
 */
class BlogPost extends RelatedContentPost implements SocialSharerLinks, PostActionCardItem
{
    const TAXONOMY = 'category';
    const RELATED_TAXONOMY = 'category';
    const POST_TYPE = 'post';

    public function getSocialSharerLinks(): array
    {
        $url = $this->permalink();
        return [
            new FacebookSocialSharer($url),
            new TwitterSocialSharer($url, $this->title()),
            new PinterestSocialSharer($url, $this->excerpt() ?: $this->title()),
            new LinkedInSocialSharer($url)
        ];
    }

    public function getTagline()
    {
        return '';
    }

    public function getCommentsNumber()
    {
        return get_comments_number($this->ID);
    }

    protected function getLandscapeImage()
    {
        return WP_Image::get_by_attachment_id($this->field('landscape_image_id'));
    }
}
