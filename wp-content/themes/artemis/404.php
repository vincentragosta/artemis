<?php

use Backstage\Util; ?>

<section class="content-section content-section--hero anchor content-section--has-bg content-section--mb-double">
    <span class="content-background-image">
        <span class="content-background-image__images">
            <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url(<?= Util::getAssetPath('images/404-page.jpg'); ?>);"></span>
        </span>
    </span>
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column col-12">
                <div class="content-column__inner"></div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-double content-section--width-narrow">
    <div class="content-section__container container">
        <div class="content-row row">
            <div class="content-column text--center col-12">
                <div class="content-column__inner">
                    <h1 class="heading heading--large">Sorry! The page you were looking for could not be found.</h1>
                    <p>Please navigate back to the homepage by clicking <a href="/">here</a>.</p>
                </div>
            </div>
        </div>
    </div>
</section>
