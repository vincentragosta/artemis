<?php

use Backstage\Models\Page;
use ChildTheme\BlogPost\BlogPost;
use ChildTheme\Components\PostActionCard\PostActionCardView;

global $wp_query, $post;
$Blog = new Page(get_option('page_for_posts'));
?>
<?php if ($content = $Blog->content(false)): ?>
    <?= $content; ?>
<?php endif ?>
<?php if ($wp_query->have_posts()): ?>
    <section class="content-section content-section--has-bg content-section--dark content-section--tpad-double content-section--bpad-double content-section--mb-none">
        <div class="content-section__container container">
            <div class="content-row row masonry">
                <?php while($wp_query->have_posts()): the_post(); ?>
                    <div class="content-column col-12 col-sm-6 col-xl-4">
                        <div class="content-column__inner">
                            <?= new PostActionCardView(new BlogPost($post)); ?>
                        </div>
                    </div>
                <?php endwhile; ?>
            </div>
            <?php if ($wp_query->found_posts > $wp_query->query_vars['posts_per_page']): ?>
                <div class="content-row row">
                    <div class="content-column text--center col-md-12">
                        <div class="content-column__inner">
                            <?php the_posts_pagination(); ?>
                        </div>
                    </div>
                </div>
            <?php endif; ?>
        </div>
    </section>
<?php endif; ?>
