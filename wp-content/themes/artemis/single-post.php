<?php

use Backstage\SetDesign\Icon\IconView;
use ChildTheme\BlogPost\BlogPost;
use ChildTheme\Components\CommentCard\CommentCardView;
use ChildTheme\Components\PostActionCard\PostActionCardView;
use ChildTheme\Support\PreferredDimensionsHandler;

$BlogPost = BlogPost::createFromGlobal();
?>

<div class="content-section content-section--width-full content-section--mb-double">
    <div class="container-fluid content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12">
                <div class="content-column__inner">
                    <?php if (($landscape_image = $BlogPost->landscape_image) instanceof \WP_Image): ?>
                        <?= $landscape_image
                            ->width(PreferredDimensionsHandler::DETAIL_WIDTH)
                            ->height(PreferredDimensionsHandler::DETAIL_HEIGHT); ?>
                    <?php elseif (($image = $BlogPost->featuredImage()) instanceof \WP_Image): ?>
                        <?= $image
                            ->width(PreferredDimensionsHandler::DETAIL_WIDTH)
                            ->height(PreferredDimensionsHandler::DETAIL_HEIGHT); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="sidebar-template">
    <div class="sidebar-template__container container">
        <div class="sidebar-template__main">
            <section class="content-section content-section--mb-half">
                <div class="content-section__container container">
                    <div class="content-row row">
                        <div class="content-column col-md-12">
                            <div class="content-column__inner">
                                <?php if ($title = $BlogPost->title()): ?>
                                    <h1 class="heading heading--medium"><?= $title; ?></h1>
                                <?php endif; ?>
                                <ul class="list list--inline list--dotted">
                                    <li>Posted on <?= $BlogPost->publishedDate(PostActionCardView::DATE_FORMAT); ?> by <span class="text--contrast"><?= $BlogPost->author()->display_name; ?></span></li>
                                    <li><?= $BlogPost->getCommentsNumber(); ?> Comments</li>
                                </ul>
                                <?php if (!empty($social_sharer = $BlogPost->getSocialSharerLinks())): ?>
                                    <ul class="list--inline">
                                        <?php foreach ($social_sharer as $SocialSharer): ?>
                                            <li><?= $SocialSharer; ?></li>
                                        <?php endforeach; ?>
                                        <li>
                                            <a href="mailto:?subject=Check out this post from Live Love Artemis!&body=It's called <?= $BlogPost->title(); ?>, copy and paste this into your browser, <?= $BlogPost->permalink(); ?> to view it.">
                                                 <?= new IconView(['icon_name' => 'email']); ?>
                                            </a>
                                        </li>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <?php if ($content = $BlogPost->content(false)): ?>
                <?= $content; ?>
            <?php endif; ?>
            <section class="content-section">
                <div class="content-section__container container">
                    <div class="content-row row">
                        <div class="content-column col-12">
                            <div class="content-column__inner">
                                <?php if (!empty($social_sharer = $BlogPost->getSocialSharerLinks())): ?>
                                    <ul class="list--inline">
                                        <?php foreach ($social_sharer as $SocialSharer): ?>
                                            <li><?= $SocialSharer; ?></li>
                                        <?php endforeach; ?>
                                        <li>
                                            <a href="mailto:?subject=Check out this post from Live Love Artemis!&body=It's called <?= $BlogPost->title(); ?>, copy and paste this into your browser, <?= $BlogPost->permalink(); ?> to view it.">
                                                <?= new IconView(['icon_name' => 'email']); ?>
                                            </a>
                                        </li>
                                    </ul>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <section class="content-section" id="comments">
                <div class="content-section__container container">
                    <div class="content-row row">
                        <div class="content-column col-12">
                            <div class="content-column__inner">
                                <?php comment_form([
                                    'logged_in_as' => '',
                                    'title_reply' => 'Drop a comment'
                                ], $BlogPost->ID); ?>
                            </div>
                        </div>
                    </div>
                    <div class="content-row row">
                        <div class="content-column col-md-12">
                            <div class="content-column__inner">
                                <?php if (!empty($comments = get_comments(['post_id' => $BlogPost->ID]))): ?>
                                    <?php foreach($comments as $Comment): ?>
                                        <?= new CommentCardView($Comment); ?>
                                    <?php endforeach; ?>
                                <?php endif; ?>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
        <div class="sidebar-template__sidebar">
            <section class="content-section content-section--mb-half">
                <div class="content-section__container container">
                    <div class="content-row row">
                        <?php dynamic_sidebar('Primary Sidebar'); ?>
                    </div>
                </div>
            </section>
        </div>
    </div>
</div>
