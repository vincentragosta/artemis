addAction(INIT, function() {
    var NARROW_BREAKPOINT = 345;
    var EXPANSE_BREAKPOINT = 768;
    var NARROW_CLASS = 'responsive-component--narrow';
    var EXPANSE_CLASS = 'responsive-component--expanse';
    var $components = $('.responsive-component');

    if (!$components.length) {
        return;
    }

    function toggleComponentClasses(components) {
        components.each(function (key, component) {
            var $component = $(component);
            if ($component.width() === 0) {
                return;
            }
            $component.toggleClass(NARROW_CLASS, $component.width() <= NARROW_BREAKPOINT);
            $component.toggleClass(EXPANSE_CLASS, $component.width() >= EXPANSE_BREAKPOINT);
        });
    }

    toggleComponentClasses($components);
    addAction(LAYOUT, function() {
        toggleComponentClasses($components);
    });
});
