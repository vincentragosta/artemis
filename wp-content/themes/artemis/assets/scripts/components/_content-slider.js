addAction(INIT, function() {
    SetDesign.addFilter('content-slider/owl-settings', function(settings) {
        settings.nav = true;
        settings.dots = false;
        settings.navText = ['<span class="icon icon-direction-up"><svg><use class="icon-use" xlink:href="#icon-chevron"></use></svg></span>', '<span class="icon"><svg><use class="icon-use" xlink:href="#icon-chevron"></use></svg></span>'];
        return settings;
    });
});
