<?php

use ChildTheme\LilyDevelopmentListing\LilyDevelopmentListing;

$LilyDevelopment = LilyDevelopmentListing::createFromGlobal();
?>

<section class="content-section content-section--hero anchor content-section--has-bg content-section--mb-none">
    <span class="content-background-image">
    <span class="content-background-image__images">
        <div class="color-overlay"></div>
        <span class="content-background-image__img content-background-image__img--desktop" style="background-image: url(<?= $LilyDevelopment->landscapeImage()->orig_url; ?>);"></span></span>
    </span>
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last text--center">
                <div class="content-column__inner">
                    <h1 class="heading heading--large"><?= $LilyDevelopment->title(); ?></h1>
                    <p class="text--italic text--bold" style="max-width: 700px; margin: 0 auto;"><?= $LilyDevelopment->post()->post_excerpt; ?></p>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-double content-section--has-bg content-section--darker content-section--tpad-double content-section--bpad-double">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last text--center">
                <div class="content-column__inner">
                    <?php if ($image = $LilyDevelopment->featuredImage()): ?>
                        <?= $image->width(375); ?>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<section class="content-section content-section--mb-double">
    <div class="container content-section__container">
        <div class="content-row row">
            <div class="content-column col-md-12 content-column--last">
                <div class="content-column__inner">
                    <?php if (!empty($list_price = $LilyDevelopment->list_price)): ?>
                        <h2 class="heading heading--medium">Starting at <?= $LilyDevelopment->list_price; ?></h2>
                    <?php endif; ?>
                    <ul class="list list--inline text--contrast">
                        <?php if (!empty($beds = $LilyDevelopment->beds)): ?>
                            <li><?= $beds; ?> BED</li>
                        <?php endif; ?>
                        <?php if (!empty($baths = $LilyDevelopment->baths)): ?>
                            <li><?= $baths; ?> BATH</li>
                        <?php endif; ?>
                        <?php if (!empty($square_footage = $LilyDevelopment->square_footage)): ?>
                            <li><?= $square_footage; ?> SQ FT</li>
                        <?php endif; ?>
                    </ul>
                    <?php if ($content = $LilyDevelopment->content(false)): ?>
                        <?= $content; ?>
                    <?php endif; ?>
                    <?php if (!empty($email = $LilyDevelopment->listingAgentEmail())): ?>
                        <a href="mailto:<?= $email; ?>" class="button button--styled">Request Custom Finishes eBook</a>
                    <?php endif; ?>
                </div>
            </div>
        </div>
    </div>
</section>
<?php if (!empty($images = $LilyDevelopment->images)): ?>
    <section class="content-section content-section--mb-double content-section--has-bg content-section--darker content-section--tpad-double content-section--bpad-double">
        <div class="container content-section__container">
            <div class="content-row row">
                <?php foreach($images as $image): ?>
                    <div class="content-column col-md-4 content-column--last text--center">
                        <div class="content-column__inner">
                            <?= \WP_Image::get_by_attachment_id($image['image']); ?>
                        </div>
                    </div>
                <?php endforeach; ?>
            </div>
        </div>
    </section>
<?php endif; ?>
<?php if (!empty($LilyDevelopment->listing_agent)): ?>
    <section class="content-section content-section--mb-double content-section--width-narrow">
        <div class="container content-section__container">
            <div class="content-row row">
                <?php if ($listing_image = $LilyDevelopment->listingAgentImage()): ?>
                    <div class="content-column col-md-6 content-column--last">
                        <div class="content-column__inner">
                            <?= $listing_image; ?>
                        </div>
                    </div>
                <?php endif; ?>
                <div class="content-column col-md-6 align-self-center content-column--last">
                    <div class="content-column__inner">
                        <?php if ($listing_agent_name = $LilyDevelopment->listingAgentName()): ?>
                            <h2 class="heading heading--default"><?= $listing_agent_name; ?></h2>
                        <?php endif; ?>
                        <?php if ($listing_agent_phone = $LilyDevelopment->listingAgentPhone()): ?>
                            <p><a href="tel:<?= $listing_agent_phone; ?>"><?= $listing_agent_phone; ?></a></p>
                        <?php endif; ?>
                        <?php if ($listing_agent_email = $LilyDevelopment->listingAgentEmail()): ?>
                            <p><a href="mailto:<?= $listing_agent_email; ?>"><?= $listing_agent_email; ?></a></p>
                        <?php endif; ?>
                        <?php if ($listing_agent_bio = $LilyDevelopment->listingAgentBio()): ?>
                            <p><?= $listing_agent_bio; ?></p>
                        <?php endif; ?>
                        <?php if ($listing_agent_phone = $LilyDevelopment->listingAgentPhone()): ?>
                            <p><a href="tel:<?= $listing_agent_phone; ?>" class="button button--styled">Call to Schedule Virtual Tour</a></p>
                        <?php endif; ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
<?php endif; ?>
