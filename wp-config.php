<?php

// ==============================
// Composer autoloader if present
// ==============================
if (file_exists(__DIR__ . '/wp-content/vendor/autoload.php')) {
    define('USE_COMPOSER_AUTOLOADER', true);
    require_once __DIR__ . '/wp-content/vendor/autoload.php';
}

// ===================================================
// Load database info and local development parameters
// ===================================================
if (file_exists(__DIR__ . '/local-config.php')) {
    include(__DIR__ . '/local-config.php');
}

// ===================================================
// Initialize Situation defaults
// ===================================================
if (class_exists('\Situation\WPConfig')) {
    new \Situation\WPConfig(__DIR__);
}

// ================================================
// You almost certainly do not want to change these
// ================================================
define('DB_CHARSET', 'utf8mb4');
define('DB_COLLATE', '');

// ==============================================================
// Salts, for security
// Grab these from: https://api.wordpress.org/secret-key/1.1/salt
// ==============================================================
define('AUTH_KEY',         '{g%Y -N@%_f= jz&n:=/y:qDe?sKWCnwz,a#x(jkj}--:$NWL*G ZR/[3h^{T0*i');
define('SECURE_AUTH_KEY',  'hCpJ^{~{Mnb;V_VQJa*T( wsw_?N7sQ)@}g%#+*ngL :V+)=~<+`|Fnk6z~{H-O-');
define('LOGGED_IN_KEY',    'qNp^`8f8EV>i[.:9Gb4L-`+;{AIzV H7vLi<,~9^R,$)B8)Ksd|&[o]1|UH_T;9X');
define('NONCE_KEY',        'ySH|&g[k6xMQ,M_SUl;-pf!-D>|;xmG}zu.!UV>R.)^XM,```;TxG1Vb(JJ1u{h-');
define('AUTH_SALT',        '`C,qXU~c~k7C,CsiZ*=0R]hO?`@(KYs-+G1-h#DPwXF*skPY?M=+?DW]S5X$DU62');
define('SECURE_AUTH_SALT', '+g4bT<RN!@O.!ed{VCL5/94JRVL+!wqcx#{U.K.hrpgK~.tY[o}2ZqUV4eh,*5-8');
define('LOGGED_IN_SALT',   '&1XX+>~ObvzK=F4r<Ce|k@^D>mn5j!P#po}!pAya5o#,X)ZR|2/Z?B/SA;h>}|Cf');
define('NONCE_SALT',       '~<R,x>~-eR;KcCo;lKMl`,$-q`cue<)IAde_*[{~Q1QEL`b5YXRk5o}UvrLjpZ()');


// ==============================================================
// Table prefix
// Change this if you have multiple installs in the same database
// Changing default to 'sit_' to enhance security
// ==============================================================
$table_prefix = 'sit_';

// ================================
// Language
// Leave blank for American English
// ================================
define('WPLANG', '');

// ===================
// Bootstrap WordPress
// ===================
if (! defined('ABSPATH')) {
    define('ABSPATH', dirname(__FILE__) . '/wp/');
}
require_once(ABSPATH . 'wp-settings.php');
